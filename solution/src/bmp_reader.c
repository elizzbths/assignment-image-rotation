#include <stdbool.h>
#include <stdio.h>

#include "bmp_reader.h"

static enum read_status verify_bmp_header(struct bmp_header bmp_header){
    if(bmp_header.bfType != BMP_TYPE) return READ_INVALID_HEADER;
    if(bmp_header.biBitCount != BMP_BIT_COUNT) return READ_INVALID_BITS;
    return READ_OK;
}

static uint32_t calculate_padding(uint32_t image_width){
    return (4 - image_width * sizeof (struct pixel) % 4) % 4;
}

static enum read_status read_image_pixels(struct image image, FILE* input_image_file){
    for(uint32_t i = 0; i < image.height; i++){
        if(fread(image.data + i * image.width, sizeof (struct pixel), image.width, input_image_file) < image.width) return READ_FILE_READ_ERROR;
        if(fseek(input_image_file, calculate_padding(image.width), SEEK_CUR)) return READ_FILE_READ_ERROR;
    }
    return READ_OK;
}

enum read_status read_bmp_image(struct image* image, FILE* input_image_file){
    // read bmp header
    struct bmp_header bmp_header = {0};
    if(input_image_file == NULL) return READ_FILE_READ_ERROR;

    if(!fread(&bmp_header, sizeof (struct bmp_header), 1, input_image_file)) return READ_FILE_READ_ERROR;

    // check if header is valid
    enum read_status header_check_status = verify_bmp_header(bmp_header);
    if(header_check_status) return header_check_status;

    bool status = true;
    *image = create_image(bmp_header.biWidth, bmp_header.biHeight, &status);
    if(!status) return READ_IMAGE_CREATE_ERROR;

    // read pixels
    enum read_status image_read_pixels_status = read_image_pixels(*image, input_image_file);
    if(image_read_pixels_status) return image_read_pixels_status;

    return READ_OK;
}

static struct bmp_header generate_bmp_header(struct image image){
    const uint32_t header_size = sizeof(struct bmp_header);
    const uint32_t image_size = sizeof(struct pixel) * image.height * (image.width + calculate_padding(image.width));
    const uint32_t file_size = header_size + image_size;

    struct bmp_header bmp_header = {
        .bfType = BMP_TYPE,
        .bfileSize = file_size,
        .bOffBits = header_size,
        .biSize = BMP_HEADER_SIZE,
        .biWidth = image.width,
        .biHeight = image.height,
        .biPlanes = 1,
        .biBitCount = BMP_BIT_COUNT,
    };

    return bmp_header;
}

static enum write_status write_image_pixels(const struct image image, FILE* output_image_file){
    uint32_t b = 0;
    for(uint32_t i = 0; i < image.height; i++){
        if(fwrite(image.data + i * image.width, sizeof (struct pixel), image.width, output_image_file) != image.width) return WRITE_FILE_ERROR;
        if(!fwrite(&b, 1, calculate_padding(image.width), output_image_file)) return WRITE_FILE_ERROR;
    }
    return WRITE_OK;
}

enum write_status write_bmp_image(const struct image image, FILE* output_image_file){

    if(output_image_file == NULL) return WRITE_FILE_ERROR;

    // create bmp header
    struct bmp_header bmp_header = generate_bmp_header(image);

    //write BMP header to file
    if(!fwrite(&bmp_header, sizeof (struct bmp_header), 1, output_image_file)) return WRITE_HEADER_ERROR;

    enum write_status pixels_write_status = write_image_pixels(image, output_image_file);
    if(pixels_write_status) return pixels_write_status;

    return WRITE_OK;
}
