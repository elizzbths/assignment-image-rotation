#include "transform.h"

struct image rotate90degree(const struct image source_image, bool* status){
    struct image rotated_image = create_image(source_image.height, source_image.width, status);
    if(source_image.data == NULL){
        *status = false;
        return (struct image) {0};
    }

    for (uint64_t i = 0; i < source_image.height; i++) {
        for (uint64_t j = 0; j < source_image.width; j++) {
            rotated_image.data[(j * rotated_image.width + source_image.height - i) - 1] = source_image.data[i * source_image.width + j];
        }
    }

    return rotated_image;
}
