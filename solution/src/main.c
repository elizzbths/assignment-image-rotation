#include <stdio.h>

#include "bmp_reader.h"
#include "transform.h"

#define requirement_arg_count 3

enum exit_status{
    SUCCESS,
    READ_IMAGE_ERROR,
    ROTATE_IMAGE_ERROR,
    WRITE_IMAGE_ERROR,
};

int main( int argc, char** argv ) {

    if(argc != requirement_arg_count) {
        printf("Invalid arguments. Usage: ./image-transformer <source-image> <transformed-image>");
        return 5;
    }

    FILE* input_image_file = fopen(argv[1], "rb");
    if(input_image_file == NULL) {
        printf("Cannot open input file");
        return READ_IMAGE_ERROR;
    }

    FILE* output_image_file = fopen(argv[2], "wb");
    if(output_image_file == NULL) {
        printf("Cannot open output file");
        return WRITE_IMAGE_ERROR;
    }

    struct image image = {0};

    const enum read_status readStatus = read_bmp_image(&image, input_image_file);
    if(readStatus){
        printf("File reading error\n");
        free_image(image);
        fclose(input_image_file);
        fclose(output_image_file);
        return READ_IMAGE_ERROR;
    }

    bool status = true;
    struct image rotated_image = rotate90degree(image, &status);
    if(!status){
        free_image(image);
        free_image(rotated_image);
        fclose(input_image_file);
        fclose(output_image_file);
        printf("Cannot rotate image\n");
        return ROTATE_IMAGE_ERROR;
    }

    const enum write_status writeStatus =  write_bmp_image(rotated_image, output_image_file);
    if(writeStatus){
        printf("File reading error\n");
        free_image(image);
        free_image(rotated_image);
        fclose(input_image_file);
        fclose(output_image_file);
        return WRITE_IMAGE_ERROR;
    }

    free_image(image);
    free_image(rotated_image);
    fclose(input_image_file);
    fclose(output_image_file);

    return SUCCESS;
}
