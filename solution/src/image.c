#include "image.h"

void free_image(struct image image){
    if(image.data != NULL)
        free(image.data);
}

struct image create_image(const uint32_t width, const uint32_t height, bool* status){

    struct image image = {
            .width = width,
            .height = height,
            .data = malloc(sizeof (struct pixel) * width * height)
    };

    if(image.data == NULL) *status = false;
    else *status = true;

    return image;
}
