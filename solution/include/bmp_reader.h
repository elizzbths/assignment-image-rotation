#include <stdint.h>
#include <stdlib.h>

#include "image.h"

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_READER_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_READER_H

#define BMP_TYPE 0x4d42
#define BMP_BIT_COUNT 24
#define BMP_HEADER_SIZE 40

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_FILE_READ_ERROR,
    READ_IMAGE_CREATE_ERROR
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_FILE_ERROR,
};

enum read_status read_bmp_image(struct image* image, FILE* input_image_file);
enum write_status write_bmp_image(const struct image image, FILE* output_image_file);


#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_READER_H
