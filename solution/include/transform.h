#include "image.h"

#ifndef IMAGE_TRANSFORMER_TRANSFORM_H
#define IMAGE_TRANSFORMER_TRANSFORM_H

struct image rotate90degree(const struct image image, bool* status);

#endif //IMAGE_TRANSFORMER_TRANSFORM_H
