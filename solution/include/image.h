#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void free_image(struct image image);
struct image create_image(const uint32_t width, const uint32_t height, bool* status);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
